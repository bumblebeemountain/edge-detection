#!/usr/bin/python3

import cv2
import numpy as np
import math

cam = cv2.VideoCapture(0)

cv2.namedWindow("Edge Detector")

def cleanImage(inputImage):
    sizeOfImage = inputImage.shape
    newImage = np.zeros((sizeOfImage[0], sizeOfImage[1], 1), np.uint8)
    cover = ((1, 2, 1), (2, 4, 2), (1, 2, 1))
    totalweight = 0

    for i in cover:
        for j in i:
            totalweight += j

    for i in range(image.shape[0]):
        for j in range(image.shape[1]):
            #pixel is inputImage[i][j]
            sumOfPixel = 0
            for x in [-1, 0, 1]:
                for y in [-1, 0, 1]:
                    if not(i+x < 0 or j+y < 0 or i+x >= sizeOfImage[0] or j+y >= sizeOfImage[1]):
                        multOfPixel = cover[x+1][y+1] * inputImage[i+x][j+y]
                        sumOfPixel += multOfPixel
            sumOfPixel = sumOfPixel / totalweight
            newImage[i][j] = sumOfPixel
    return newImage

def applyKernel(kernel, inputImage):
    newImage = np.zeros((inputImage.shape[0], inputImage.shape[1]), np.float)
    sizeOfkernel = len(kernel)
    for rowIndex, row in enumerate(inputImage):
        for columnIndex, element in enumerate(row):
            sumOfPixel = 0
            pixelVal = 0
            for i in [-1, 0, 1]:
                for j in [-1, 0, 1]:
                    imageCoord = (rowIndex+i, columnIndex+j)
                    matrixCoord = (i+1, j+1)
                    if not(imageCoord[0] < 0 or imageCoord[1] < 0 or imageCoord[0] >= len(image) or imageCoord[1] >= len(image)):
                        pixelVal = image[imageCoord[0]][imageCoord[1]]
                    matrixVal = kernel[matrixCoord[0]][matrixCoord[1]]
                    sumOfPixel += int(pixelVal * matrixVal)
            newImage[rowIndex][columnIndex] = sumOfPixel
    return newImage

def combineFilters(xfilter, yfilter):
    newImage = np.zeros((xfilter.shape[0], xfilter.shape[1], 1), np.uint8)
    for i in range(xfilter.shape[0]):
        for j in range(xfilter.shape[1]):
            valueOfPixel = int(math.sqrt((int(xfilter[i][j]))**2 + (int(yfilter[i][j]))**2))
            newImage[i][j] = valueOfPixel
    return newImage

def printMatrix(matrix):
    for row in matrix:
        print(row)

def lowerResolution(matrix):
    scale = 4
    newImage = np.zeros((matrix.shape[0]//scale, matrix.shape[1]//scale), np.float)
    for i in range(0, matrix.shape[0]-1, scale):
        for j in range(0, matrix.shape[1]-1, scale):
            newImage[i//scale][j//scale] = matrix[i][j]
    return newImage

def findEdge(matrix, xkernel, ykernel):
    xOutput = applyKernel(xkernel, matrix)
    print("x")
    yOutput = applyKernel(ykernel, matrix)
    print("y")
    combined = combineFilters(xOutput, yOutput)
    print("combined")
    return combined

sizeOfSquare = 480

coverx = ((-1, 0, 1), (-1, 0, 1), (-1, 0, 1))
# sobelx = ((-3, 0, 3), (-10, 0, 10), (-3, 0, 3))
# sobelx = np.array([[-3, 0, 3], [-10, 0, 10], [-3, 0, 3]], dtype = np.float)

covery = ((-1, -1, -1), (0, 0, 0), (1, 1, 1))
# sobely = ((-3, -10, -3), (0, 0, 0), (3, 10, 3))

blur = np.array([[1, 1, 1], [1, 1, 1], [1, 1, 1]], dtype = np.float)
# blur = ((1, 1, 1), (1, 1, 1), (1, 1, 1))


if __name__ == '__main__':
    while True:
        ret, frame = cam.read()
        if not ret:
            break

        centre = (int(frame.shape[0]/2), int(frame.shape[1]/2))

        startx = int(centre[0]-sizeOfSquare//2)
        endx = int(centre[0]+sizeOfSquare//2)
        starty = int(centre[1]-sizeOfSquare//2)
        endy = int(centre[1]+sizeOfSquare//2)

        croppedImage = frame[startx:endx, starty:endy]
        image = cv2.cvtColor(croppedImage, cv2.COLOR_BGR2GRAY)
        cv2.imshow("Edge Detector", image)
        k = cv2.waitKey(1)

        if k%256 == 27:
            # ESC pressed
            print("Escape hit, closing...")
            break

        elif k%256 == 32:
            # SPACE pressed
            print("running")

            cleanedImage = cleanImage(image)
            print("cleaned")
            edge = findEdge(cleanedImage, coverx, covery)

            cv2.imshow("image", edge)
            cv2.imshow("original", image)

    cam.release()
    cv2.destroyAllWindows()
